var mongo = require('mongodb').MongoClient;
var Handlebars=require('handlebars');
var url = 'mongodb://localhost:27017/mydb';
var express = require('express');
var router = express.Router();


router.get('/', function(req, res) {
    mongo.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        dbo.collection("InfoTable").find({}).toArray(function (err, result) {
            if (err) throw err;
            res.render('Home',{dataTable:result});
            db.close();
        });
    });

});
router.post('/add', function(req, res) {
    var input= req.body.input;
    mongo.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        var myObj = { data: input };
        dbo.collection("InfoTable").insertOne(myObj, function(err, res2) {
            if (err) throw err;
            console.log("1 document inserted");
            res.redirect('/');
            db.close();
        });
    });

});

module.exports = router;





